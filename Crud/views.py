from .models import Post
from rest_framework import viewsets
from .serializers import PostSerializer


class PostView(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
